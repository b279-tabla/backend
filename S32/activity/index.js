// // - An alternative solution is to use the command "npx kill-port [port-number]" (e.g npx kill-port 4000).
let http = require("http");

http.createServer(

    function(req, res)
    {

        // GET method
        if(req.url == "/" && req.method == "GET"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Welcome to booking system")
        }
        if(req.url == "/profile" && req.method == "GET"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Welcome to your profile")
        }
        if(req.url == "/courses" && req.method == "GET"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Here's our courses available")
        }
        
        // POST method
        if(req.url == "/addCourse" && req.method == "POST"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Add course to our resources")
        }

        // PUT method
        if(req.url == "/updateCourse" && req.method == "PUT"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Update a course to our resources")
        }

        // DELETE method
        if(req.url == "/archiveCourse" && req.method == "DELETE"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Archive courses to our resources")
        }

    }
).listen(4000);

console.log("Server running at localhost:4000");

// Kindly See output folder for the output of this Code. Thank you so much.