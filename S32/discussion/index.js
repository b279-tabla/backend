
// // - An alternative solution is to use the command "npx kill-port [port-number]" (e.g npx kill-port 4000).
let http = require("http");

http.createServer(

    function(req, res)
    {
        // CRUD -> R -> RETRIEVE
        // GET method is somehow equal to Retrieve method in CRUD
        if(req.url == "/items" && req.method == "GET"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Data retrieved from the database.")
        }

        // POST method is use for creating/sending data to our dataase
        if(req.url == "/items" && req.method == "POST"){
            res.writeHead(200, {"Content-Type" : "text/pplain"});
            res.end("Data to be sent to the databse.")
        }
    }
).listen(4000);

console.log("Server running at localhost:4000");