// console.log("Hello World!")

//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

console.log("");
console.log("Exponent Operator");
// Exponent Operator

const number = 2;
const getCube = number * number * number;
console.log(getCube);


console.log("");
console.log("Template Literals");
// Template Literals

console.log(`The cube of ${number} is ${getCube}`);


console.log("");
console.log("Array Destructuring");
// Array Destructuring

const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}`);


console.log("");
console.log("Object Destructuring");
// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal;

console.log(`${animal.name} was a ${animal.species}. he ${animal.weight} with a measurement of ${animal.measurement}.`)

console.log("");
console.log("Arrow Functions");


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach(function(numbers){
    console.log(`${numbers}`);
})

const reduceNumber = (num1, num2, num3, num4, num5) => num1 + num2 + num3 + num4 + num5;

let total = reduceNumber(1, 2, 3, 4, 5);

console.log(total);


console.log("");
console.log("Javascript Classes");
// Javascript Classes

class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog();

myDog.name = "Chinchin";
myDog.age = 3;
myDog.breed = "shih tzu";

console.log(myDog);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}