
// Activity S31 Question and Answer

/* 
1. What directive is used by Node.js in loading the modules it needs?

    - require()

2. What Node.js module contains a method for server creation?

    - http module

3. What is the method of the http object responsible for creating a server using Node.js?

    - createServer()

4. What method of the response object allows us to set status codes and content types?
	
    - response.writeHead()

5. Where will console.log() output its contents when run in Node.js?

    - terminal

6. What property of the request object contains the address's endpoint?

    - request.url 

*/

const http =  require("http");

// Create a variable "port" to store the port number
const port = 3000;

const server = http.createServer(
    (request, response) => 
    {
        // Accessing the "greeting" route returns a message of "Hello Again"
	    // "request" is an object that is sent via the client (browser)
	    // The "url" property refers to the url or the link in the browser
        if(request.url == "/login"){
            response.writeHead(200, {"Content-Type" : "text/plain"});
            response.end("Welcome to the login page.");
        }
        else{
            response.writeHead(404, {"Content-Type" : "text/plain"});
            response.end("I'm sorry the page you are looking for cannot be found.");
        }
    }
);

server.listen(port);
console.log(`Server is sucessfully running`);

// for running the terminal "npx nodemon activty.js"
