// Use "require" directive to load Node.js modules
// A "http module" lets Node.js transfer data using Hyper Text Transfer Protocol
// Clients (browser) and servers (node JS/Express JS application) communicate by exchanging individual messanges.

let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listents to requests on specified port.

// A port is a virtual point where network connection start and end.
// Each port is associated with a specific process or service.
// The server will be assigned to port 4000.

/* 
    HTTP response status codes
        Informational responses (100 – 199)
        Successful responses (200 – 299)
        Redirection messages (300 – 399)
        Client error responses (400 – 499)
        Server error responses (500 – 599)
*/

http.createServer(
    function (request, response)
    {
        // Use writeHead() method to:
        // Set a status code for the message - 200 means ok
        // Set the content-type of the response as a plain text
        
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Hello World!");

        // Open Browser and type localhost:400
        // "response.end" body of the browser
    }

).listen(4000);

console.log("Server running at localhost: 4000");
// Open git bash in S31 directory type node index.js
// Do not cancel or exit git bash since it is needed to generate the function
