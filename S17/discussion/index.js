// console.log("Hello World!");

// [SECTION] Function
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
// Functions are mostly created to create complicated tasks to run several lines of code in succession
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// function keyword - used to defined a js function
// functionName - function are named to be able to use later in the code.
// function block ({}) - the statement which comprise the body of the function.
// This is where the code to be executed.
/* 
Function functionName(){
    // code block to be executed
}
 */


console.log("Hello!");

function printName(){
    console.log("My Name is John");
}

// Invocation or function calling
printName();
printName();
printName();

//  Function Invocation
// The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
// It is common to use the term "call a function" instead of "invoke a function".

// printName(); -> this is the function Invocation.

// declaredFunction(); -> will result  an error because we cannot call a function that is not defined/declared.

// hoisting -> calling a function before its declaration
declaredFunction();


function declaredFunction(){
    console.log("Hello World from declaredFunction()");
}

declaredFunction();

// Function Expression
//A function can also be stored in a variable. This is called a function expression.

//A function expression is an anonymous function assigned to the variableFunction

//Anonymous function - a function without a name.

let variableFunction = function(){
    console.log("Hello Again!");
}

variableFunction();

// We can also create a function expression of a named function.
// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
// Function Expressions are always invoked (called) using the variable name.

let functionExpression = function funcName(){
    console.log("Hello from the other side.");
}

// funcName(); -> will result an error
functionExpression();

// Re-assigning a function expression

declaredFunction = function(){
    console.log("Updated declaredFunction");
}

declaredFunction();

funcExpression = function(){
    console.log("Updated funcExpression");
}

functionExpression();
functionExpression();
functionExpression();

const constantFunc = function(){
    console.log("Initialized with const!");
}

constantFunc();

/* constantFunc = function(){
    console.log("Connot be Updated");
}

constantFunc();

Will result an error, cannot update a function stored in a const variable. 
*/
/*	
Scope is the accessibility (visibility) of variables within our program.
	
Javascript Variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/

function showNames(){
    // Function Scope Variables
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();

//  Will Result an Error cannot read variable inside the function
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

let globalVar = "Mr. WorldWide";

{
    let localVar = "Armando Perez";
    console.log(localVar);
    //  this will work because the declaration and the usage of the variable is at the same scope.
    console.log(globalVar);
}

// console.log(localVar); will cause error becasue the variable is enclose with the curly braces or it is locally declared in curly braces.

// Nested Function
function myNewFunction(){
    let name = "Jane";

    function nestedfunction(){
        let nestedName = "John";
        console.log(name);
    }
    // console.log(nestedName); -> will result an error, cannot access function scoped variable.
    nestedfunction();
}

myNewFunction();
// nestedfunction(); -> will result an error

// Function and Global Scope Variables

// Global Variable
let globalName = "Alexandro";

function myNewFunction2(){
    let nameInside = "Renz";

    console.log(globalName);
    // Global variable can be accessed inside a function
}

myNewFunction2();

//  [SECTION] Return Statement
// To use return statement we will be using "return" keyword

function returnFullName(){
    console.log("Hello");
    return "Jeffrey Smith Bezos";
    // console.log("Hello");
    // console.log("Hello");
    // console.log("Hello");
    // console.log("Hello"); will never run because the return statement was declared before this line of codes.
}

let fullName = returnFullName();
console.log(fullName);

console.log(returnFullName());

function returnFullAddress(){
    let fullAdress = {
        street: "44 Maharlika St.",
        city: "Cainta",
        province: "Rizal"
    }

    return fullAdress;
}

let myAddress = returnFullAddress();
console.log(myAddress);

function printPlayerInfo(){
    console.log("username: " + "White_knight");
    console.log("Level:" + 95);
    console.log("Job: " + "Paladin");
}

let user1 = printPlayerInfo();
console.log(user1);

// You can return any data types from a function

function returnSumOf5and10(){
    return 5 + 10;
}

let sum5and10 = returnSumOf5and10();
console.log(sum5and10);

let total = 100 + returnSumOf5and10();
console.log(total);

// Simulates getting an array of user names from a DB

function getGuildMembers(){
    return ["white_knight",  "healer2000", "Masterthief100"];
}

console.log(getGuildMembers());

// Function Naming Convention
//  Function names should be definitive of the task it will perform.

function getCourses(){
    let courses = ["Science", "Math", "English"];

    return courses;
}

let courses = getCourses();
console.log(courses);

// Avoid generic names

/* function get(){

}

function foo(){
    
} */