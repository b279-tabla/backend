/* 
    Create a hotel database and create several documents for rooms.
    Manipulate the data  using MongoDB CRUD operations.
*/

// 3.Insert a single room (insertOne method) in the rooms collection:

// db.rooms.insertOne({});
db.rooms.insertOne({
    name: "single",
    accommodates: 2,
    price: 100,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
})

// We added single rooms



// 4.Insert a multiple room (insertMany method) in the rooms collection:

// db.rooms.insertMany([{}, {}, ...]);
db.rooms.insertMany([
    {
        name: "double",
        accommodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        rooms_available: 5,
        isAvailable: false
    },
    {
        name: "queen",
        accommodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false
    }
]);

// We added double and queen rooms



// 5.Use the find method to search for a room with the name double

// db.rooms.findOne({});
db.rooms.findOne({name: "double"});

// output shoud be only double rooms



// 6.Use the update method to update the queen room and set the available rooms to 0

// db.rooms.updateOne({{criterial},{$set:{...}});

db.rooms.updateOne(
    {name: "queen"},
    {$set:
        {rooms_available: 0}
    }
);

//  queen room has no room available since we update the rooms_available from 15 to 0




// 7. Use the deleteMany method to delete all rooms that have 0 rooms available.

// db.rooms.deleteMany({});
db.rooms.deleteMany(
    {
        rooms_available: 0
    }
);

//  queen rooms is deleted since it has 0 rooms available