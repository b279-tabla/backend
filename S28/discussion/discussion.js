
// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [SECTION] (Create) Inserting Documents
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
    firstName: "James",
    lastName: "Tabla",
    age: 24,
    contact: {
        phone: "09054869321",
        email: "james.ray.p.tabla@gmail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
}); 

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

//  Insert Many
/* 
Syntax
- db.collectionName.insertMany([{objectA}, {objectB}]) 
*/

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "09054869321",
            email: "stephenhawking@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "09054869321",
            email: "neilArmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
]);


// FInding a single document 
// Leaving the search criteria empty will retrive all documents

// SELECT * FROM users;


db.users.find();

// SELECT * FORM users WHERE firstname = "Stephen";

db.users.find({ firstName: "Stephen" });


//  The "pretty" method allows us to be able to view documents returned by ourr terminal in a better format

db.users.find({ firstName: "Stephen" }).pretty();

// Finding documents with multiple parameters

db.users.find({ lastName: "Armstrong" }, { age: 82});

// [SECTION] (Update) Updating Documents 

// Updating Single Document

// Creating dummy user to update
db.users.insertOne({
    firstName: "Test",
    lastName: "test",
    age: 0,
    contact: {
        phone: "00000000000",
        email: "text@gmail.com"
    },
    courses: [],
    department: "none"
}); 

/* 
Syntax

db.collectionName.updateOne({criterial}, {$set:{field: value}});

*/

db.users.updateOne(
    {firstName: "Test" },
    {$set:{
        firstName:"Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "091234566789",
            email: "bill@mail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
        }
    }   
);

// Updating Many Documents

db.users.updateMany(
    { department: "none"},
    {$set: {
        department: "HR"
    }}
);

// Replace One
/* 
Syntax

db.collectionName.replaceOne({criteria}, {field: value});

*/


db.users.replaceOne(
    { firstName: "Bill" },
    {
        firstName:"Bill",
        lastName: "Gates",
        age: 65,
            contact: {
                phone: "091234566789",
                email: "bill@mail.com"
            },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations"
    }
);

// [SECTION] (Delete) Deleting Documents

db.users.insertOne({
    firstName: "Test"
});

// Delete One
db.users.deleteOne({
    firstName: "Test"
});




// Delete Many 
db.users.deleteMany({
    firstName: "Test"
});


// [SECTION] Advance Queries

db.users.find({
    contact: {
        phone: "09054869321",
        email: "stephenhawking@gmail.com"
    }
});

// Query on nested field
db.users.find({"contact.email": "stephenhawking@gmail.com"});

// Querying ann Array without regard to order

db.users.find({course: {$all: ["React", "Python"]}});
db.users.find({course: {$all: ["React"]}});

// Querying and embeded Array

db.users.insertOne({
    nameArray: [{nameA: "Juan"}, {nameB: "Tamad"}]
});