// console.log("Hello!")

// 4. Using data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => { 
    let titleMap = title.map((dataToMap) => dataToMap.title)
    console.log(titleMap);
});

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(result => console.log(result));


// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON PLaceholder API.
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        userId: 1,
        title: "Created To Do List Item",
    })
}).then(res => res.json())
.then(json => console.log(json));


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        dateComplted: "Pending",
        title: "Updated To Do List Item",
        status: "Pending",
        description: "To update the my to do list with a different data structure",
        userId: 1
    })
}).then(res => res.json())
.then(json => console.log(json));


// 9. Update a to do list item by changing the data structure to contain the following properties:
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        dateCompleted: "07/09/21",
        title: "delectus aut autem",
        status: "Complete",
        description: "To update the my to do list with a different data structure",
        userId: 1
    })
}).then(res => res.json())
.then(json => console.log(json));

// 10. Create a fetch request  using PATCH method that will update a to do list item using JSON Placeholder API

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        title: "PATH Method"
    })
}).then(res => res.json())
.then(json => console.log(json));

// 11. Updated a to do list item by changing the status to complete and add a date when the status was changed.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {"Content-Type" : "application/json"},
    body: JSON.stringify({
        title: "PATH Method",
        dateCompleted: "07/09/21",
        title: "delectus aut autem",
    })
}).then(res => res.json())
.then(json => console.log(json))

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE"});










