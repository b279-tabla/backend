//  console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here
for(let i = 0; i < string.length; i++){

    let vowel = string[i].toLowerCase();

    if (vowel >= 'a' && vowel <= 'z' && !('aeiou'.includes(vowel))){
        filteredString += vowel;
    }
}

console.log(filteredString);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}