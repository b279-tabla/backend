/* 
Using MongoDB Query Operators and Field Projection to retrieve documents from our database 
*/



// 2. Find users with letter s in their first name or d in their last name.

/* 
a. Use the $or operator
      - db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
      - Case insensitivty query query {$regex: ".", $option: "i"} 
*/

db.users.find(
    {$or:
        [
            {firstName: {$regex: "s", $options: "i"}},
            {lastName: {$regex: "d", $options: "i"}}
        ]
    }
);

// b. Show only the firstName and lastName fields and hide the _id field.

/*
Add Suppressing ID field
      - Case insensitivty query query {$regex: ".", $option: "i"}
      - db.users.find({criteria},{field: 1, field 0}) 
*/

db.users.find(
    {$or:
        [
            {firstName: {$regex: "s", $options: "i"}},
            {lastName: {$regex: "d", $options: "i"}}
        ]
    },
    {
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);



// 3. Find users who are from the HR department and their age is greater than or equal to 70

/*
 a. Use the $and operator
      - db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
      - {$gte:..} greater than or equal
*/

db.users.find(
    {$and:
        [
			{department: "HR"}, 
			{age: {$gte: 70}}
		]
    }
);



// 4. Find users with the letter e in their first name and has an age of less than or equal to 30.

/* 
a. Use the $and, $regex and $lte operator.
    - db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
     - Case sensitivty query query {$regex: "..."}
     - {$lte:..} less than or equal 
*/


db.users.find(
    {$and:
        [
            {firstName: {$regex: "e"}},
            {age: {$lte: 30}}
        ]
    }
);