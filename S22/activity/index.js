// console.log("Hello World!")
/*
    Create functions which can manipulate our arrays.
*/

/*
    Important note: Don't pass the arrays as an argument to the function. 
    The functions must be able to manipulate the current given arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function called register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, return the message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and return the message:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function register(){
    console.log(registeredUsers);
    return
}
 register();

function user(username){
    let registeredFound1 = registeredUsers.includes(username);
if(registeredFound1){
    console.log( "register("+ username +")")
    console.log('Registration Failed. User already exists!')
}else{
    console.log( "register("+ username +")")
    console.log('Thank you for registering!')
    registeredUsers.push(username);
}}

  user("James Jeffries");

  user("Conan O' Brien");

  user("Conan O' Brien");

/*
    2. Create a function called addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then return the message with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, return the message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
function register(){
    console.log(registeredUsers);
    return
}

register();


function user1(username){
    let registeredFound2 = registeredUsers.includes(username);
if(registeredFound2){
    console.log( "addFriend("+ username +")");
    console.log('You have added ' + username +" as a friend!");
    friendsList.push(username);
}else{
    console.log( "addFriend("+ username +")");
    console.log('User not found.');
}}

  user1("Victor Magtanggol");

  user1("Conan O' Brien");

console.log(friendsList);



/*
    3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty return the message: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

// Uncomment this for proving of 0 friend list!

/* 
function deleteAll(){
    friendsList = [];
    return
}
deleteAll();
console.log(friendsList); 
*/

function displayFriends(){
    if (friendsList.length > 0){
        console.log("displayFriends()");
        console.log(friendsList);
    }
   else{
   console.log("displayFriends()");
   console.log('You have 0 friendsList. Add one friendsList.');
   }
}

displayFriends();


/*
    4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty return the message:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty return the message:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

// Uncomment this for proving of 0 friend list!
/* 
function deleteAll(){
    friendsList = [];
    return
}
deleteAll();
console.log(friendsList); 
 */


function displayNumberOfFriends(){
    if (friendsList.length > 0){
        console.log(friendsList);
        console.log("displayNumberOfFriends()");
        console.log("You currently have " + friendsList.length + " friends.")
    }
   else{
   console.log("displayFriends()");
   console.log('You have 0 friendsList. Add one friendsList.');
   }
}

displayNumberOfFriends();

/*
    5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty return a message:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

let deleteFriend = friendsList.pop();
console.log( friendsList);

function displayNumberOfFriends1(){
    if (friendsList.length > 0){
        console.log(friendsList);
        console.log("displayNumberOfFriends()");
        console.log("You currently have " + friendsList.length + " friends.")
    }
   else{
   console.log("displayFriends()");
   console.log('You have 0 friendsList. Add one friendsList.');
   }
}

displayNumberOfFriends1();


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

/* let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
]; */

// Removing Gunther Smith placed in index 1


registeredUsers.splice(1, 1);
console.log("Mutated array from splice method:");
console.log(registeredUsers);


//For exporting to test.js
try{
    module.exports = {

        registeredUsers: typeof registeredUsers !== 'undefined' ? registeredUsers : null,
        friendsList: typeof friendsList !== 'undefined' ? friendsList : null,
        register: typeof register !== 'undefined' ? register : null,
        addFriend: typeof addFriend !== 'undefined' ? addFriend : null,
        displayFriends: typeof displayFriends !== 'undefined' ? displayFriends : null,
        displayNumberOfFriends: typeof displayNumberOfFriends !== 'undefined' ? displayNumberOfFriends : null,
        deleteFriend: typeof deleteFriend !== 'undefined' ? deleteFriend : null

    }
} catch(err){

}